#!/usr/bin/env bash

curl -s https://gitlab.com/ZzenlD/zbfl/-/raw/master/zbfl.sh -o /tmp/zbfl.sh
source /tmp/zbfl.sh
script_name="ZzenlD's Setup"
logfile=""
config_file=""
column_width='75'
dependencies=("git" "grep" "ssh" "ssh-import-id-gh")
repositories=("gitlab.com/ZzenlD/envc"
    "gitlab.com/ZzenlD/exporter"
    "gitlab.com/ZzenlD/proxmoxwol-vm")

prepare_zbfl
{
    init_zbfl

    # Prepare ssh-key, check ssh-agent forwarding
    write_headline "" "Requirements"
    write_ask 1 "Import ssh-key from GitHub" "-IMPORT_SSH_GH" no yesno
    if [[ "${IMPORT_SSH_GH}" == yes ]]; then
        write_ask 2 "Username" "-GH_USERNAME" ZzenlD
        write_topic "" "Import key"
        ssh-import-id-gh "${GH_USERNAME}" &>/dev/null
        write_status "$?"
    fi
    write_topic 1 "SSH agent forwarding"
    if [[ -n "$SSH_AUTH_SOCK" ]]; then
        write_status 0 ok
    else
        write_status 1 failed
    fi

    write_headline 0 "Getting started"
    # Select repository
    for repository in ${repositories[@]}; do
        repository_options="${repository_options}${repository}, "
    done
    write_ask 1 "Select repository" "-REPOSITORY_FULL" "" options "${repository_options%, }"
    host="${REPOSITORY_FULL%%/*}"
    username="$(echo "${REPOSITORY_FULL#*/}" | sed "s/\\/.*//")"
    repository="${REPOSITORY_FULL##*/}"

    # Create directory
    write_ask "" "Storage path" "-STORAGE_PATH" "$HOME/${repository}/git_repo"
    if ! [[ -d "${STORAGE_PATH}" ]]; then
        mkdir -p "${STORAGE_PATH}"
    fi
    write_topic "" "Import ssh-fingerprint from ${host}"
    ssh-keygen -F "${host}" &>/dev/null || ssh-keyscan "${host}" &>/dev/null >>~/.ssh/known_hosts
    write_status "$?"
    write_topic "" "Clone ${REPOSITORY_FULL}"
    git clone git@"${host}":"${username}"/"${repository}".git "${STORAGE_PATH}" &&
        cd "${STORAGE_PATH}"
    write_status $?

    # Select branch
    for branch in $(git branch -r --format='%(refname:short)' | grep -v HEAD); do
        branch_options="${branch_options}${branch}, "
    done
    write_ask "" "Select branch" "-BRANCH" "$(git branch -r --format='%(refname:short)' | grep -oh 'origin/m.*')" options "${branch_options%%, }"
    git checkout "${BRANCH}" &>/dev/null

    # Start setup.sh if available
    if [[ -f setup.sh ]]; then
        write_ask "" "Execute setup.sh of ${username}/${repository}" "-EXECUTE_SETUP" "yes" "yesno"
        if [[ "${EXECUTE_SETUP}" == yes ]]; then
            chmod +x setup.sh
            ./setup.sh
        fi
    else
        write_message "" error "${REPOSITORY_FULL} in branch ${BRANCH} does not contain setup.sh"
    fi

    write_result final
} 2> >(write_log)
